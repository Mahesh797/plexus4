/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

static void
join_cycle(struct PlexusClient *c, int numeric)
{
  irc_join(c, "#a");
  if (numeric)
    expect_numeric(c, numeric);
  else
  {
    expect_message(c, c->client, "JOIN");
    irc_part(c, "#a");
    expect_message(c, c->client, "PART");
  }
}

static void
join_key_cycle(struct PlexusClient *c, const char *key, int numeric)
{
  irc_join_key(c, "#a", key);
  if (numeric)
    expect_numeric(c, numeric);
  else
  {
    expect_message(c, c->client, "JOIN");
    irc_part(c, "#a");
    expect_message(c, c->client, "PART");
  }
}

static void
join_no_part(struct PlexusClient *c)
{
  irc_join(c, "#a");
  expect_message(c, c->client, "JOIN");
}

static void
part_routine(struct PlexusClient *c)
{
  irc_part(c, "#a");
  expect_message(c, c->client, "PART");
}

static void
set_expect_mode(struct PlexusClient *setter, struct Channel *chptr, const char *modes)
{
  irc_client_set_cmode(setter, chptr, modes);
  expect_message(setter, setter->client, "MODE");
}

static void
send_invite(struct PlexusClient *c1, struct PlexusClient *c2, struct Channel *chptr)
{
  irc_invite(c1, chptr, c2->client);
  expect_numeric(c1, RPL_INVITING);
  expect_message(c2, c1->client, "INVITE");
}

START_TEST(join)
{
  plexus_up();

  struct PlexusClient *client1 = client_register("test1"),
                      *client2 = client_register("test2");

  SetCanFlood(client1->client);
  SetCanFlood(client2->client);

  // Temporary, until we have client_register_ssl()
  client1->client->flags  |= FLAGS_SSL;
  client1->client->umodes |= (UMODE_REGISTERED|UMODE_SSL);
  strlcpy(client1->client->suser, "chanop", sizeof(client1->client->suser));

  ++Count.oper;
  SetOper(client1->client);

  join_no_part(client1);

  struct Channel *chptr = hash_find_channel("#a");
  ck_assert_ptr_ne(chptr, NULL);

  const struct Membership *client1_ms = NULL;
  ck_assert_ptr_ne((client1_ms = find_channel_link(client1->client, chptr)), NULL);

  // Make sure we got op'd
  ck_assert(has_member_flags(client1_ms, CHFL_CHANOP));

  // Test joining under +b
  set_expect_mode(client1, chptr, "+b test2!*@*");
  join_cycle(client2, ERR_BANNEDFROMCHAN);
  // Give client2 a ban exemption
  set_expect_mode(client1, chptr, "+e test2!*@*");
  join_cycle(client2, 0);
  // Remove the ban exemption and make sure they can't rejoin
  set_expect_mode(client1, chptr, "-e test2!*@*");
  join_cycle(client2, ERR_BANNEDFROMCHAN);
  set_expect_mode(client1, chptr, "-b test2!*@*");

  // Test joining under +i
  set_expect_mode(client1, chptr, "+i");
  join_cycle(client2, ERR_INVITEONLYCHAN);
  // Give client2 an invex
  set_expect_mode(client1, chptr, "+I test2!*@*");
  join_cycle(client2, 0);
  // Remove the invex and make sure they can't rejoin
  set_expect_mode(client1, chptr, "-I test2!*@*");
  join_cycle(client2, ERR_INVITEONLYCHAN);
  set_expect_mode(client1, chptr, "-i");

  // Test joining under +k
  set_expect_mode(client1, chptr, "+k testkey");
  join_cycle(client2, ERR_BADCHANNELKEY);
  join_key_cycle(client2, "badkey", ERR_BADCHANNELKEY);
  join_key_cycle(client2, "testkey", 0);
  set_expect_mode(client1, chptr, "-k");

  // INVITEs cannot override these next cmodes,
  // so send one now.
  send_invite(client1, client2, chptr);

  // Test joining under +l
  set_expect_mode(client1, chptr, "+l 1");
  join_cycle(client2, ERR_CHANNELISFULL);
  set_expect_mode(client1, chptr, "-l");

  // Test joining under +R
  set_expect_mode(client1, chptr, "+R");
  join_cycle(client2, ERR_NEEDREGGEDNICK);
  set_expect_mode(client1, chptr, "-R");

  // Test joining under +S
  set_expect_mode(client1, chptr, "+S");
  join_cycle(client2, ERR_SSLONLYCHAN);
  set_expect_mode(client1, chptr, "-S");

  // Test joining under +O
  set_expect_mode(client1, chptr, "+O");
  join_cycle(client2, ERR_OPERONLYCHAN);
  set_expect_mode(client1, chptr, "-O");

  // INVITE does not overrides bans
  set_expect_mode(client1, chptr, "+b test2!*@*");
  join_cycle(client2, ERR_BANNEDFROMCHAN);
  set_expect_mode(client1, chptr, "-b test2!*@*");

  // INVITE does not override +k
  set_expect_mode(client1, chptr, "+k noinviteride");
  join_cycle(client2, ERR_BADCHANNELKEY);
  set_expect_mode(client1, chptr, "-k");

  // Make sure we can get through with +i and unset the INVITE flag
  set_expect_mode(client1, chptr, "+i");
  join_cycle(client2, 0);
  set_expect_mode(client1, chptr, "-i");

  ck_assert_ptr_eq(find_invite(chptr, client2->client), NULL);

  // Incase something got cached or stuck, run one last time
  join_cycle(client2, 0);

  // Part client1, make sure the channel deletes
  part_routine(client1);

  ck_assert_ptr_eq(hash_find_channel("#a"), NULL);
}
END_TEST

START_TEST(join_ext)
{
  plexus_up();

  struct PlexusClient *client1 = client_register("test1"),
                      *client2 = client_register("test2");

  SetCanFlood(client1->client);
  SetCanFlood(client2->client);

  join_no_part(client1);

  struct Channel *chptr = hash_find_channel("#a");
  ck_assert_ptr_ne(chptr, NULL);

  // Set a few things -
  // client->suser:  extban 'a' | Account bans
  // client->info:   extban 'r' | Gecos bans
  // client->certfp: extban 'z' | Certfp bans
  client2->client->umodes |= UMODE_REGISTERED;
  strlcpy(client2->client->suser, "badacct", sizeof(client2->client->suser));
  strlcpy(client2->client->info, "gecoext", sizeof(client2->client->info));
  client2->client->certfp = xstrdup("0759cd50ccdfa128cc07c4769dddd8ac362b393d");

  // Send an INVITE, it shouldn't allow a JOIN anyways
  send_invite(client1, client2, chptr);

  // Test joining under extended ban a:
  set_expect_mode(client1, chptr, "+b a:badacct");
  join_cycle(client2, ERR_BANNEDFROMCHAN);
  set_expect_mode(client1, chptr, "-b a:badacct");

  // Test joining under extended ban r:
  set_expect_mode(client1, chptr, "+b r:gecoext");
  join_cycle(client2, ERR_BANNEDFROMCHAN);
  set_expect_mode(client1, chptr, "-b r:gecoext");

  // Test joining under extended ban z:
  set_expect_mode(client1, chptr, "+b z:0759cd50ccdfa128cc07c4769dddd8ac362b393d");
  join_cycle(client2, ERR_BANNEDFROMCHAN);
  set_expect_mode(client1, chptr, "-b z:0759cd50ccdfa128cc07c4769dddd8ac362b393d");

  join_cycle(client2, 0);
}
END_TEST

void
join_setup(Suite *s)
{
  TCase *tc = tcase_create("join");

  tcase_add_checked_fixture(tc, NULL, plexus_down);
  tcase_add_test(tc, join);
  tcase_add_test(tc, join_ext);

  suite_add_tcase(s, tc);
}
