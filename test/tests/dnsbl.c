/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

static struct dnsbl_entry *test1, *test2;

static void
setup_dnsbl(void)
{
  struct dnsbl_entry *entry = MyMalloc(sizeof(struct dnsbl_entry));
  strlcpy(entry->name, "test1.rizon.net", sizeof(entry->name));
  dlinkAdd(entry, &entry->node, &dnsbl_items);
  test1 = entry;

  entry = MyMalloc(sizeof(struct dnsbl_entry));
  strlcpy(entry->name, "test2.rizon.net", sizeof(entry->name));
  dlinkAdd(entry, &entry->node, &dnsbl_items);
  test2 = entry;
}

START_TEST(dnsbl_test)
{
  setup_dnsbl();

  struct PlexusClient *client = client_create("test");

  ck_assert_int_eq(dlink_list_length(&test1->lookups), 1);
  ck_assert_int_eq(dlink_list_length(&test2->lookups), 1);

  struct ip_entry *ip = find_or_add_ip(&client->client->localClient->ip);
  ip->dnsbl_entry = test1;

  timeout_query_list(CurrentTime + 42);
}
END_TEST

void
dnsbl_setup(Suite *s)
{
  TCase *tc = tcase_create("dnsbl");

  tcase_add_checked_fixture(tc, plexus_up, plexus_down);
  tcase_add_test(tc, dnsbl_test);

  suite_add_tcase(s, tc);
}
