/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 Adam <Adam@anope.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

START_TEST(privmsg_flood)
{
  struct PlexusClient *client1 = client_register("test1"),
                      *client2 = client_register("test2");

  irc_join(client1, "#a");
  expect_message(client1, client1->client, "JOIN");

  irc_join(client2, "#a");
  expect_message(client2, client2->client, "JOIN");

  struct Channel *chptr = hash_find_channel("#a");
  ck_assert_ptr_ne(chptr, NULL);

  struct Membership *client1_ms = find_channel_link(client1->client, chptr);
  ck_assert_ptr_ne(client1_ms, NULL);

  client1_ms->flags = 0; // remove channel op

  GlobalSetOptions.floodcount = 2;
  GlobalSetOptions.floodtime = 60;

  irc_privmsg(client1, "#a", "test");
  expect_message(client2, client1->client, "PRIVMSG");

  irc_privmsg(client1, "#a", "test");
  expect_message(client2, client1->client, "PRIVMSG");

  ck_assert_int_eq(chptr->received_number_of_privmsgs, 2);

  irc_privmsg(client1, "#a", "test");
  expect_message_args(client1, &me, "NOTICE %s :*** Message to %s throttled due to flooding", client1->client->name, chptr->chname);

  ck_assert_int_eq(chptr->received_number_of_privmsgs, 2);

  chptr->first_received_message_time -= 61;

  irc_privmsg(client1, "#a", "test");
  expect_message(client2, client1->client, "PRIVMSG");

  ck_assert_int_eq(chptr->received_number_of_privmsgs, 1);
}
END_TEST

void
privmsg_setup(Suite *s)
{
  TCase *tc = tcase_create("privmsg");

  tcase_add_checked_fixture(tc, plexus_up, plexus_down);
  tcase_add_test(tc, privmsg_flood);

  suite_add_tcase(s, tc);
}
