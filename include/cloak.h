/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *  cloak.h: A header for the cloaking system.
 *
 *  Copyright (C) 2005 by the past and present ircd coders, and others.
 *  Copyright (c) 2017 Adam <Adam@anope.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 *
 */

#ifndef INCLUDED_cloak_h
#define INCLUDED_cloak_h

#include "ircd_defs.h"

struct cloak_config;

struct CloakSystem
{
  const char *name;
  int (*cloak)(struct cloak_config *config, const char *host, char *out, size_t sz);
  dlink_node node;
};

struct UserCloaks
{
  dlink_list cloaks;        /* user cloaks */
};

struct UserCloak
{
  /*
   * The clients cloaked IP and host. These contain the same value if
   * the user has no hostname, only an IP.
   */
  char              ip[HOSTLEN + 1];
  char              host[HOSTLEN + 1];
  dlink_node        node;
};

extern void cloak_register(struct CloakSystem *);
extern void cloak_unregister(struct CloakSystem *);
extern struct CloakSystem *cloak_find_system(const char *name);
extern void cloak_reload(void);

extern void cloak_client(struct UserCloaks *cloaks, const char *realhost, const char *sockhost);
extern void cloak_free(struct UserCloaks *cloaks);
extern int cloak_match(struct UserCloaks *cloaks, const char *mask);
extern struct UserCloak *cloak_get(struct UserCloaks *cloaks);

extern char *cloak_key;

#endif /* INCLUDED_cloak_h */
