/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2015-2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#ifndef INCLUDED_dnsbl_h
#define INCLUDED_dnsbl_h

#include "irc_res.h"

struct ip_entry;

struct DnsblLookup
{
	struct Client *cptr; /* client this lookup is for */
	dlink_node node;     /* node into cptr->dnsbl_queries */

  struct dnsbl_entry *entry; /* blacklist this query is against */
  dlink_node enode;          /* node into entry->lookups */

  dns_handle query;    /* dns query handle */
};

extern void start_dnsbl_lookup(struct Client *);
extern void clear_dnsbl_lookup(struct Client *);
extern void cancel_blacklist(struct dnsbl_entry *bl);
extern const char *dnsbl_format_reason(struct Client *, struct ip_entry *);
extern void dnsbl_ban(struct Client *, struct ip_entry *);

#endif /* INCLUDED_dnsbl_h */

