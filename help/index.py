#!/usr/bin/env python

from os import listdir

l = [f.upper() for f in sorted(listdir('.')) if 'Makefile' not in f and '.' not in f and 'index' != f]

num = 4
split = [l[i:i + num] for i in xrange(0, len(l), num)]

print "Available HELP topics:"
print ""

for pair in split:
    s = ""
    for i in range(0, len(pair)):
        if i == len(pair) - 1:
            s = s + "%s"
        else:
            s = s + "%-16s"
    print s % tuple(pair)
