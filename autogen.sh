#!/bin/sh
which autoreconf > /dev/null
if [ $? -eq 0 ] ; then
	autoreconf -fi
	if [ -f configure ] ; then
		echo "Success. Now run ./configure."
	else
		echo "There was a problem configuring Plexus. Please verify that you have autoconf installed correctly."
		exit 1
	fi
else
	echo "Plexus requires autoconf which can be downloaded at https://www.gnu.org/software/autoconf/ or through your system's package manager."
	echo "If you have installed autoconf already, ensure it is in your PATH environment variable."
	exit 1
fi